# Additive Manufacturing Research

## Instructions

Once the site.make file is copied to the site you will need to run `drush cc all`,
`drush updb -y`, `drush fra -y` and `drush cc all` to have the site use the local
uw_ct_single_page_home and the uw_theme_marketing modules. Then you will need to enable 
the uw_salesforce_block module to have the rest of the required modules enabled.


